import Adafruit_DHT
import lcd_i2c
import time
import sqlite3
from subprocess import *
from datetime import datetime


def InsertWeather(temp, humid):
    conn = sqlite3.connect('weather.db')
    curs = conn.cursor()
    curs.execute("PRAGMA synchronous=OFF")
    curs.execute("insert into weather values (strftime('%Y-%m-%d %H:%M:%f', 'now'),?,?)",[round(temp,1),round(humid,1)])
    conn.commit()
    conn.close()

def GetLANIP():
   cmd = "ip addr show eth0 | grep inet  | grep -v inet6 | awk '{print $2}' | cut -d '/' -f 1"
   p = Popen(cmd, shell=True, stdout=PIPE)
   output = p.communicate()[0]
   return output

def GetWLANIP():
   cmd = "ip addr show wlan0 | grep inet  | grep -v inet6 | awk '{print $2}' | cut -d '/' -f 1"
   p = Popen(cmd, shell=True, stdout=PIPE)
   output = p.communicate()[0]
   return output





sensor = Adafruit_DHT.DHT22

lcd_i2c.lcd_init()

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0
LCD_LINE_3 = 0x94 # LCD RAM address for the 3rd line
LCD_LINE_4 = 0xD4 # LCD RAM address for the 4th line

humidity, temperature = Adafruit_DHT.read_retry(sensor, 4)
InsertWeather(temperature,humidity)

lanip = GetLANIP().strip()
wanip = GetWLANIP().strip()

retry=0

while((not lanip) and (not wanip)) and retry<5:
    time.sleep(2)
    lanip = GetLANIP().strip()
    wanip = GetWLANIP().strip()
    retry = retry + 1

lcd_i2c.lcd_string(lanip,LCD_LINE_3)
lcd_i2c.lcd_string(wanip,LCD_LINE_4)

if humidity is not None and temperature is not None:
    lcd_i2c.lcd_string('    {0:0.1f}c {1:0.1f}%'.format(temperature, humidity),LCD_LINE_2)

while True:

        now = datetime.now()

        lcd_i2c.lcd_string(now.strftime("%Y-%m-%d %H:%M"),LCD_LINE_1)
        
        if(now.minute % 10 == 0):
            humidity, temperature = Adafruit_DHT.read_retry(sensor, 4)
            if humidity is not None and temperature is not None:
                #print 'Temp={0:0.1f}c  Humidity={1:0.1f}%'.format(temperature, humidity)
                lcd_i2c.lcd_string('    {0:0.1f}c {1:0.1f}%'.format(temperature, humidity),LCD_LINE_2)
                InsertWeather(temperature,humidity)

        time.sleep(30)
        
