# README #

I have been playing with Raspberry Pis and electronics a lot and my 4 year old son have occassionally comes to my desk to play with the red, yellow and green LEDs by sticking them on breadboards.  He called them "traffic lights",  so I thought to myself, why not build something for him which does that for Christmas!

Here's the features of the device which I have decided

A mobile App controlled traffic light which can simulate the traffic light / pedestrian light sequences and can also be manually turned on/off
Some practical features  like a 20 characters x 4 rows screen which displays
Date and time
Temperature and humidity
A message which is controlled by the app (2 lines x 20 characters)

### How do I get set up? ###

* To be added