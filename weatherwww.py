from flask import Flask
from flask import request
import lcd_i2c
import RPi.GPIO as GPIO
import time
import sqlite3
import json

app = Flask(__name__)

LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line
LCD_LINE_3 = 0x94 # LCD RAM address for the 3rd line
LCD_LINE_4 = 0xD4 # LCD RAM address for the 4th line



GPIO_RED = 37
GPIO_YELLOW = 35
GPIO_GREEN = 36
GPIO.setmode(GPIO.BOARD)
GPIO.setup(GPIO_RED,GPIO.OUT) #LED - flash when read first signal
GPIO.setup(GPIO_YELLOW,GPIO.OUT) #LED - flash when read any signal
GPIO.setup(GPIO_GREEN,GPIO.OUT) #LED - flash when read any signal

@app.route('/getweatherst')
def getweatherst():
    conn = sqlite3.connect('weather.db')
    curs = conn.cursor()
    curs.execute("PRAGMA synchronous=OFF")
    rescur = curs.execute("select * from weather order by rowid desc limit 1;")

    for row in rescur:
        d = {}
        d["temperature"] = row[1]
        d["humidity"] = row[2]
    conn.commit()
    conn.close()
    return str(d["temperature"]) + "c  " + str(d["humidity"]) + "%"



@app.route('/getweather')
def getweather():
    conn = sqlite3.connect('weather.db')
    curs = conn.cursor()
    curs.execute("PRAGMA synchronous=OFF")
    rescur = curs.execute("select * from weather order by rowid desc limit 1;")

    for row in rescur:
        d = {}
        d["temperature"] = row[1]
        d["humidity"] = row[2]
    conn.commit()
    conn.close()
    return json.dumps(d)


@app.route('/alloff')
def allledoff():
    GPIO.output(GPIO_RED,False)
    GPIO.output(GPIO_YELLOW,False)
    GPIO.output(GPIO_GREEN,False)
    return 'OK'

@app.route('/redon')
def redon():
    GPIO.output(GPIO_RED,True)
    return 'OK'


@app.route('/redoff')
def redoff():
    GPIO.output(GPIO_RED,False)
    return 'OK'

@app.route('/yellowon')
def yellowon():
    GPIO.output(GPIO_YELLOW,True)
    return 'OK'


@app.route('/yellowoff')
def yellowoff():
    GPIO.output(GPIO_YELLOW,False)
    return 'OK'

@app.route('/greenon')
def greenon():
    GPIO.output(GPIO_GREEN,True)
    return 'OK'


@app.route('/greenoff')
def greenoff():
    GPIO.output(GPIO_GREEN,False)
    return 'OK'



@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/display')
def display():
    line3 = request.args.get('line3')[:20]
    line4 = request.args.get('line4')[:20]

    
    
    lcd_i2c.lcd_string(" ".rjust(int((20-len(line3))/2)," ")+line3,LCD_LINE_3)
    lcd_i2c.lcd_string(" ".rjust(int((20-len(line4))/2)," ")+line4,LCD_LINE_4)
    return 'OK'

@app.route('/blinkyellow')
def blinkyellow():
    allledoff()
    GPIO.output(GPIO_YELLOW,True)
    time.sleep(1)
    GPIO.output(GPIO_YELLOW,False)
    time.sleep(1)
    GPIO.output(GPIO_YELLOW,True)
    time.sleep(1)
    GPIO.output(GPIO_YELLOW,False)
    time.sleep(1)
    GPIO.output(GPIO_YELLOW,True)
    time.sleep(1)
    GPIO.output(GPIO_YELLOW,False)
    return 'OK'
    
@app.route('/pedestlight')
def pedestlight():
    GPIO.output(GPIO_RED,True)
    GPIO.output(GPIO_YELLOW,False)
    GPIO.output(GPIO_GREEN,False)
    time.sleep(5)
    GPIO.output(GPIO_RED,False)
    GPIO.output(GPIO_GREEN,True)
    time.sleep(5)
    GPIO.output(GPIO_GREEN,False)
    time.sleep(1)
    GPIO.output(GPIO_GREEN,True)
    time.sleep(1)
    GPIO.output(GPIO_GREEN,False)
    time.sleep(1)
    GPIO.output(GPIO_GREEN,True)
    time.sleep(1)
    GPIO.output(GPIO_GREEN,False)
    time.sleep(1)
    GPIO.output(GPIO_GREEN,True)
    time.sleep(1)
    GPIO.output(GPIO_GREEN,False)
    time.sleep(1)
    GPIO.output(GPIO_GREEN,True)
    time.sleep(1)
    GPIO.output(GPIO_GREEN,False)
    GPIO.output(GPIO_RED,True)
    return 'OK'
    
@app.route('/trafficlight')
def trafficlight():
    GPIO.output(GPIO_RED,True)
    time.sleep(1)
    GPIO.output(GPIO_YELLOW,True)
    time.sleep(1)
    GPIO.output(GPIO_RED,False)
    GPIO.output(GPIO_YELLOW,False)
    time.sleep(0.2)
    GPIO.output(GPIO_GREEN,True)
    time.sleep(4)
    GPIO.output(GPIO_RED,False)
    GPIO.output(GPIO_YELLOW,True)
    GPIO.output(GPIO_GREEN,False)
    time.sleep(1)
    GPIO.output(GPIO_RED,True)
    GPIO.output(GPIO_YELLOW,False)
    
    return 'OK'
